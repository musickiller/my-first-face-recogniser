# mainly from https://robotos.in/uroki/obnaruzhenie-i-raspoznavanie-litsa-na-python

import cv2
"""
A script to train a recogniser, based on dataset.

Use script 03 to prepare photos. Capable of learning several people.
"""

import numpy as np
from PIL import Image
import os

# Path for face image database
path = 'dataset'

recognizer = cv2.face.LBPHFaceRecognizer_create()
#for this to work, run "python -m pip install opencv-contrib-python --upgrade"
# detector = cv2.CascadeClassifier("haarcascade_frontalface_default.xml");

# function to get the images and label data
def getImagesAndLabels(path):
    imagePaths = [os.path.join(path,f) for f in os.listdir(path)]     
    faceSamples=[]
    ids = []
    for imagePath in imagePaths:
        # PIL_img = Image.open(imagePath).convert('L') # convert it to grayscale
        PIL_img = Image.open(imagePath)
        img_numpy = np.array(PIL_img,'uint8')
        id = int(os.path.split(imagePath)[-1].split(".")[1])
        # faces = detector.detectMultiScale(img_numpy)
        # for (x,y,w,h) in faces:
            # faceSamples.append(img_numpy[y:y+h,x:x+w])
            # ids.append(id)
        faceSamples.append(img_numpy)
        ids.append(id)
        
    return faceSamples,ids

print ("\n [INFO] Training faces. It will take a few seconds. Wait ...")
faces,ids = getImagesAndLabels(path)
recognizer.train(faces, np.array(ids))

# Save the model into trainer/trainer.yml
recognizer.write('trainer/trainer.yml') # recognizer.save() worked on Mac, but not on Pi

# Print the numer of faces trained and end program
print("\n [INFO] {0} faces trained. Exiting Program".format(len(np.unique(ids))))