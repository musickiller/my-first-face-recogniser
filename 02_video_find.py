"""
This script is searching for faces on video using lbp cascade classifier.
It's faster than Haar, so I've decided to choose this one.

GUI not implemented, but can be done through this:
https://solarianprogrammer.com/2018/04/21/python-opencv-show-video-tkinter-window/
"""

#import required libraries
#import OpenCV library
import cv2
import numpy as np

# for fps fix
import time

from detectors import find_faces

target_fps = 29

# there is an error in fps correction, so I had to fix it by hand:
fps_error = 7
t_spf = 1/(target_fps-fps_error)
cap = cv2.VideoCapture('data/vid1.mp4')
face_cascade = cv2.CascadeClassifier('data/lbpcascade_frontalface.xml')


# print("checking if file is opened...")
# print (cap.isOpened())
t_start = 0.0
t_stop = 0.0
delay = 1
all_sfp = []

while(cap.isOpened()):
    ret, frame = cap.read()
    
    try:
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    except cv2.error:
        print ("cv2 error occured. Most surely, the video has ended.")
        print ("Program end.")
        break
    else:
        ## compensating high fps ##
        t_stop = time.perf_counter()
        # print ("stop at", t_stop)
        if t_start != 0:
            sfp = t_stop-t_start
            all_sfp += [sfp]
            delay = int((t_spf - sfp)*1000)
            if delay <= 0:
                delay = 1
            # print (delay)
        t_start = time.perf_counter()
        # print ("start at", t_start)
        
        ## in the case of no errors, search for faces ##
        faces = find_faces(face_cascade, gray)
        
        ## faces in squares ##
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
        cv2.imshow('frame',frame)
        
    ## wait for a key and compensate for high fps ##
    # 0x001B is unicode for Esc key
    if cv2.waitKey(delay) & 0xFF in [ord('q'), 0x001B, ord(' ')]:
        break

        
cap.release()
cv2.destroyAllWindows()

print ("calculate avg fps:")
avg_sfp = sum(all_sfp) / float(len(all_sfp))
avg_fps = 1/avg_sfp
print (avg_fps)