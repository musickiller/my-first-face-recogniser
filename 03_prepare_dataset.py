"""
A smaller script to prepare my dataset.

1. Put photos containing target's face in 'path' (defaults to './photos_to_process')
    (only one face per photo, only .jpg)
2. Enter target's id (integer)
3. Faces will be detected and photos will be cropped and converted to grayscale.
    Output folder: 'dataset_path', defaults to './dataset'
    Old photos will be saved and new will be added, so don't put the same photo
    twice.
"""

import cv2

import os
import glob

from detectors import find_faces, smart_find

path = 'photos_to_process'
dataset_path = "dataset"
face_id = input('\n enter user id (integer) ==>  ')
count = len(glob.glob(os.path.join(dataset_path,"user."
                                        + str(face_id) + ".*.jpg")))+1
# for filename in os.listdir(path):
    # print(filename)
    
# print("===============")

for filename in glob.glob(os.path.join(path, '*.jpg')):
    print("Processing a photo:", filename)
    img = cv2.imread(filename)
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    face_cascade = cv2.CascadeClassifier('data/haarcascade_frontalface_default.xml')
    face = smart_find(face_cascade, img_gray)
    for (x,y,w,h) in face:
        cv2.imwrite(dataset_path + "/user." + str(face_id) + '.'
            + str(count) + ".jpg", img_gray[y:y+h,x:x+w])
    count += 1
    