# A small script I used to mass-rename my dataset

import os

id = "1"
path = 'dataset'


imagePaths = [os.path.join(path,f) for f in os.listdir(path)]
for imagePath in imagePaths:
    number = os.path.split(imagePath)[-1].split(".")[2]
    new_name = path + "/user."+ id +"." + number + ".jpg"
    # print(new_name)
    print(imagePath, "-->", new_name)
    os.rename(imagePath, new_name)