"""
My first attempt at face-detection. Quite a success, I think.

Capable of detecting face, eyes, smile (if you don't use glasses at least).
Will continue to adjust parameters till it less than finds 2 smile and 3 eyes.
So please smile once and close all third eyes.

mostly based on https://www.superdatascience.com/blogs/opencv-face-detection
"""

##### IMPORTS #####

#import required libraries
#import OpenCV library
import cv2

#import matplotlib library (not required)
# import matplotlib.pyplot as plt

#importing time library for speed comparisons of both classifiers
# import time

# for jupyter:
# %matplotlib inline


##### FUNCTIONS #####
# OpenCV loads images into BGR color space by default
# helper function to convert image to RGB space
def convertToRGB(img): 
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    
    
##### PARAMETERS #####
#load test iamge
img_folder = "data"
# img_name = "pp.jpg"
# img_name = "IMG_scarf.jpg"
# img_name = "many.jpg"
img_name = "m9MbYgL95baCYLZJGy8DUxk=.jpg"
# img_name = "IMG_20190805_192947.jpg"
# img_name = "IMG_20190724_122938.jpg"
# img_name = "IMG_20190816_141424.jpg"
# img_name = "IMG_20190816_141518.jpg"
# img_name = "IMG_20190724_123107.jpg"


##### INIT #####
test1 = cv2.imread(img_folder+"/"+img_name)
#convert the test image to gray image as opencv face detector expects gray images
gray_img = cv2.cvtColor(test1, cv2.COLOR_BGR2GRAY)

# get image h/w
img_h, img_w = gray_img.shape[:2]
print ("image is loaded")
print ("  height =", img_h)
print ("  width =", img_w)

# plt.imshow(gray_img, cmap='gray')

# or display the gray image using OpenCV
# cv2.imshow('Test Imag', gray_img)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

    
#load cascade classifier training file for haarcascade
# face_cascade = cv2.CascadeClassifier('data/haarcascade_frontalface_default.xml')
# face_cascade = cv2.CascadeClassifier('data/lbpcascade_frontalface_improved.xml')
face_cascade = cv2.CascadeClassifier('data/lbpcascade_frontalface.xml')
haar_eye_cascade = cv2.CascadeClassifier('data/haarcascade_eye.xml')
haar_smile_cascade = cv2.CascadeClassifier('data/haarcascade_smile.xml')

# face_cascade = lbp_face_cascade


##### MAIN PART #####

### FIND FACES ###


# detect a face from an image using the CascadeClassifier we just loaded

"""
detectMultiScale(image, scaleFactor, minNeighbors): This is a general function to detect objects, in this case, it'll detect faces since we called in the face cascade. If it finds a face, it returns a list of positions of said face in the form “Rect(x,y,w,h).”, if not, then returns “None”.

Image: The first input is the grayscale image. So make sure the image is in grayscale.

scaleFactor: This function compensates a false perception in size that occurs when one face appears to be bigger than the other simply because it is closer to the camera.

minNeighbors: This is a detection algorithm that uses a moving window to detect objects, it does so by defining how many objects are found near the current one before it can declare the face found.
"""

#let's detect multiscale (some images may be closer to camera than others) images
# lovered minNeighbors to get higher result numbers

#           common params:
maxSize=(int(img_h*0.8), int(img_w*0.85))
#           big_face_params
minSize=(int(img_h*0.1), int(img_w*0.1))
face_scaleFactor=1.1
minNeighbors=5
#           small_faces_params
# face_scaleFactor=1.02
# minNeighbors=12
# minSize = (10,10)

faces = face_cascade.detectMultiScale(gray_img, scaleFactor=face_scaleFactor,
                                        minNeighbors=minNeighbors,
                                        minSize=minSize,
                                        maxSize=maxSize)

#print the number of faces found
print('Faces found: ', len(faces))

### CUT IMAGE TO FACES ###

#go over list of faces and draw them as rectangles on original colored
face_imgs = []
for (x, y, w, h) in faces:
    img = cv2.rectangle(test1, (x, y), (x+w, y+h), (0, 255, 0), 2)
    crop_img = gray_img[y:y+h, x:x+w]
    face_imgs += [crop_img]
    
    
# crop image to the found face and search for other features:
# crop_img = gray_img[y:y+h, x:x+w]
# cv2.imshow("cropped", crop_img)
# cv2.waitKey(0)
    
    
### FIND OTHER FEATURES ON FACES ###

def detect_facial_features(cascade, face_img, face_position,
                            max_features, min_features = 0, scaleFactor = 1.1):
    xf, yf, wf, hf = face_position
    precision = 1

i = 0
for face_img in face_imgs:
    print(f"checking face {i} for eyes and smiles...")
    xf, yf, wf, hf = faces[i]
    # face_position = faces[i]
    num_features = 4
    # precision = 20
    precision = 1
    print ("  Checking for eyes")
    while num_features > 2:
        print("  Precision is", precision)
        eyes = haar_eye_cascade.detectMultiScale(
                                                    face_img,
                                                    scaleFactor=1.1,
                                                    minNeighbors=precision
                                                )
        num_features = len(eyes)
        precision = precision + int(num_features/2)
        print('  Eyes found: ', num_features)
    for (x, y, w, h) in eyes:
        img = cv2.rectangle(test1, (x+xf, y+yf), (x+xf+w, y+yf+h), (0, 0, 255), 2)
        
    print ("  Checking for smiles")
    num_features = 2
    # precision = 60
    precision = 1
    while num_features > 1:
        print("  Precision is", precision)
        smiles = haar_smile_cascade.detectMultiScale(face_img, scaleFactor=1.1, minNeighbors=precision, minSize=(30, 30))
        num_features = len(smiles)
        precision = precision + int(num_features/1.1)
        print('  Smiles found: ', num_features)
    for (x, y, w, h) in smiles:
        img = cv2.rectangle(test1, (x+xf, y+yf), (x+xf+w, y+yf+h), (255, 0, 0), 2)
    print (eyes)
    print (smiles)
    i += 1
    

    

    

    
# display the result
cv2.imshow('Test Imag', test1)
cv2.waitKey(0)
cv2.destroyAllWindows()