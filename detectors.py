"""
A module, containing various detectors.

find_faces - a small helper function, used by me for face detection.
smart_find - a smart (and slow) function, that will try different options
    to find targeted features, depending on the outcome.
"""

import cv2

# модуль числа - absolute value
# from math import abs

def find_faces(cascade, gray_img, scaleFactor=1.1):
    
    img_h, img_w = gray_img.shape[:2]
    
    #           common params:
    maxSize=(int(img_h*0.8), int(img_w*0.85))
    #           big_face_params
    minSize=(int(img_h*0.3), int(img_w*0.3))
    face_scaleFactor=1.2
    minNeighbors=4
    #           small_faces_params
    # face_scaleFactor=1.02
    # minNeighbors=12
    # minSize = (10,10)
    # next was "faces = ..."
    return cascade.detectMultiScale(gray_img, scaleFactor=scaleFactor,
                                        minNeighbors=minNeighbors,
                                        minSize=minSize,
                                        maxSize=maxSize)
                                        
def smart_find(cascade, gray_img,
                num_features = 1,
                max_iterations = 20,
                maxSize = None, minSize = None,
                scaleFactor=1.2,
                start_minNeighbors=4):
                
                
    img_h, img_w = gray_img.shape[:2]
    
    if maxSize == None:
        maxSize=(int(img_h*0.8), int(img_w*0.85))
    if minSize == None:
        minSize=(int(img_h*0.3), int(img_w*0.3))
    
    minNeighbors = start_minNeighbors
    
    for i in range(max_iterations):
        detected = cascade.detectMultiScale(gray_img, scaleFactor=scaleFactor,
                                        minNeighbors=minNeighbors,
                                        minSize=minSize,
                                        maxSize=maxSize)
        
        if len(detected) == num_features:
            break
        
        print("detected", len(detected), "instead of", num_features)
        jumpyness = (abs(len(detected) - num_features))
        print ("'jumpyness' = ", jumpyness)
        if jumpyness < 1:
            jumpyness = 1 
        
        if len(detected) > num_features:
            minNeighbors += jumpyness
        elif len(detected) < num_features:
            minNeighbors -= jumpyness
        else:
            print ("ERROR: should not happen!")
    
    return detected