#!python36
"""
Can't finish this in time. EmoPy requires tensorflow==1.12.0 which requires
    Python 3.6 (I have 3.7).
    
It's almost finished, by the way... May even work, I just can't check it.
"""

import cv2
import numpy as np
import os 

from EmoPy.src.fermodel import FERModel
# from pkg_resources import resource_filename

target_emotions = ['calm', 'anger', 'happiness']
emo_model = FERModel(target_emotions, verbose=True)

recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read('trainer/trainer.yml')
cascadePath = "data/haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath);

font = cv2.FONT_HERSHEY_SIMPLEX
# ..который шрифт, а не fRont

#iniciate id counter
id = 0

# names related to ids: example ==> Marcelo: id=1,  etc
names = ['None', 'Master'] 

# Initialize and start realtime video capture
# cam = cv2.VideoCapture(0)
# cam.set(3, 640) # set video widht
# cam.set(4, 480) # set video height

cam = cv2.VideoCapture('data/vid1.mp4')
tmp_file = 'image.jpg'

# Define min window size to be recognized as a face
minW = 0.1*cam.get(3)
minH = 0.1*cam.get(4)
# minW = 50
# minH = 50

while True:
    ret, img =cam.read()
    # img = cv2.flip(img, -1) # Flip vertically
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    
    faces = faceCascade.detectMultiScale( 
        gray,
        scaleFactor = 1.2,
        minNeighbors = 5,
        minSize = (int(minW), int(minH))
        # minSize = (30,30)
        )

    for(x,y,w,h) in faces:
        # draw a rectangle
        cv2.rectangle(img, (x,y), (x+w,y+h), (0,255,0), 2)
        # recognise a person
        #Save the captured frame on disk
	
        cv2.imwrite(tmp_file, gray[y:y+h,x:x+w])
        # id, confidence = recognizer.predict(gray[y:y+h,x:x+w])
        # recognise an emotion
        # emo_model.predict(resource_filename('EmoPy.examples','image_data/sample_happy_image.png'))
        # frameString = emo_model.predict(gray[y:y+h,x:x+w])
        
        # ^ this ^ should fail. The project example tells to save image to disk,
        #   and predict with it. Which is a poor choice yet still an option.
        
        frameString = model.predict(file)
        
        # put text on a frame
        cv2.putText(frame, frameString, (10, 35), fontFace, fontScale, (255, 255, 255), thickness, cv2.LINE_AA)

        # Check if confidence is less them 100 ==> "0" is perfect match 
        if (confidence < 100):
            id = names[id]
            confidence = "  {0}%".format(round(100 - confidence))
        else:
            id = "unknown"
            confidence = "  {0}%".format(round(100 - confidence))
        
        cv2.putText(img, str(id), (x+5,y-5), font, 1, (255,255,255), 2)
        cv2.putText(img, str(confidence), (x+5,y+h-5), font, 1, (255,255,0), 1)  
    
    cv2.imshow('camera',img) 

    k = cv2.waitKey(10) & 0xff # Press 'ESC' for exiting video
    if k == 27:
        break

# Do a bit of cleanup
print("\n [INFO] Exiting Program and cleanup stuff")
cam.release()
cv2.destroyAllWindows()